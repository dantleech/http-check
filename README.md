HTTP Health Check
==================

Simple application to crawl a HTTP endpoint and dump the results to a CSV in
addition to showing real-time status and showing the top 10 slowest links.
