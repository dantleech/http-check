<?php

namespace DTL\HttpHealthCheck\Core;

class CrawlerFactory
{
    public function create($url, $depth = 4)
    {
        return new Crawler($url, $depth);
    }
}
