<?php

namespace DTL\HttpHealthCheck\Core;

use Iterator;
use IteratorAggregate;
use Traversable;

class TopResults implements IteratorAggregate
{
    /**
     * @var Iterator
     */
    private $iterator;

    /**
     * @var array
     */
    private $stack = [];

    /**
     * @var int
     */
    private $size;

    /**
     * @var string
     */
    private $key;

    public function __construct(Traversable $iterator, string $key, int $size = 10)
    {
        $this->iterator = $iterator;
        $this->size = $size;
        $this->key = $key;
    }

    /**
     * {@inheritDoc}
     */
    public function getIterator()
    {
        foreach ($this->iterator as $result) {
            $this->stack[] = $result;

            usort($this->stack, function ($a, $b) {
                return $b[$this->key] <=> $a[$this->key];
            });

            // discard oldest entry
            if (count($this->stack) > $this->size) {
                array_shift($this->stack);
            }

            yield $result;
        }
    }

    public function topResults(): array
    {
        return $this->stack;
    }

}
