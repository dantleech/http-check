<?php

namespace DTL\HttpHealthCheck\Application;

use DTL\HttpHealthCheck\Application\Command\CheckCommand;
use DTL\HttpHealthCheck\Core\CrawlerFactory;
use Symfony\Component\Console\Application;

class Container
{
    public function application(): Application
    {
        $application = new Application();
        $application->add(new CheckCommand($this->crawlerFactory()));

        return $application;
    }

    public function crawlerFactory(): CrawlerFactory
    {
        return new CrawlerFactory();
    }
}
