<?php

namespace DTL\HttpHealthCheck\Core;

class CsvWriter
{
    private $handle;
    private $headerWritten = false;

    public function __construct(string $file)
    {
        $this->handle = fopen($file, 'w');
    }

    public function write(array $info)
    {
        $info = $this->flattenInfo($info);
        if ($this->headerWritten === false) {
            fputcsv($this->handle, array_keys($info));
            $this->headerWritten = true;
        }

        fputcsv($this->handle, $info);
    }

    public function close()
    {
        fclose($this->handle);
    }

    private function flattenInfo(array $info, $prefix = '')
    {
        foreach ($info as $key => $value) {
            $newKey = $prefix . $key;

            if (is_array($value)) {
                unset($info[$key]);
            }
        }

        return $info;
    }

}
