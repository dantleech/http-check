<?php

namespace DTL\HttpHealthCheck\Application\Command;

use DTL\HttpHealthCheck\Core\CrawlerFactory;
use DTL\HttpHealthCheck\Core\CsvWriter;
use DTL\HttpHealthCheck\Core\TopResults;
use Stringy\Stringy;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CheckCommand extends Command
{
    /**
     * @var CrawlerFactory
     */
    private $crawlerFactory;

    public function __construct(CrawlerFactory $crawlerFactory)
    {
        parent::__construct();
        $this->crawlerFactory = $crawlerFactory;
    }

    public function configure()
    {
        $this->setName('check');
        $this->addArgument('url', InputArgument::REQUIRED, 'Base URL to crawl');
        $this->addArgument('output', InputArgument::REQUIRED, 'Write CSV file to this URL');
        $this->addOption('depth', null, InputOption::VALUE_REQUIRED, 'Maximimum-depth');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $crawler = $this->crawlerFactory->create($input->getArgument('url'), $input->getOption('depth'));
        $output->writeln('Crawling...');
        $output->write(PHP_EOL);

        $section = $output->section();
        $table = new Table($section);
        $table->setHeaders([
            'URL',
            'Status',
            'Depth',
            'Time',
        ]);

        $csvWriter = new CsvWriter($input->getArgument('output'));

        $topResults = new TopResults($crawler, 'total_time');

        foreach ($topResults as $info) {
            $csvWriter->write($info);
            $table->appendRow([
                'url' => 
                    '<comment>' . Stringy::create($info['referrer'])->truncate(60, '...') . '</>' . PHP_EOL .
                    '  => ' . Stringy::create($info['url'])->truncate(60, '...'),
                'code' => $info['http_code'] !== 200 ? '<error>' . $info['http_code'] . '</>' : $info['http_code'],
                'depth' => $info['depth'],
                'total_time' => $info['total_time'],
            ]);
        }

        $csvWriter->close();

        $output->write(PHP_EOL);
        $output->writeln('Slowest URLs');
        $output->write(PHP_EOL);
        $table = new Table($output);

        foreach ($topResults->topResults() as $topResult) {
            $table->addRow([
                'url' => Stringy::create($topResult['url'])->truncate(60, '...'),
                'code' => $topResult['http_code'],
                'total_time' => $topResult['total_time'],
            ]);
        }
        $table->render();
    }
}
