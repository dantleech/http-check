<?php

namespace DTL\HttpHealthCheck\Core;

use Generator;
use IteratorAggregate;
use Symfony\Component\DomCrawler\Crawler as DomCrawler;
use Webmozart\PathUtil\Path;
use Webmozart\PathUtil\Url;

class Crawler implements IteratorAggregate
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var array
     */
    private $seenUrls = [];

    /**
     * @var int
     */
    private $depth;

    public function __construct(string $url, int $depth = 4)
    {
        $this->url = $url;
        $this->depth = $depth;
    }

    public function getIterator()
    {
        yield from $this->crawlUrl($this->url, $this->url);
    }

    private function crawlUrl(string $url, string $referrer = null, $depth = 0): Generator
    {
        if (isset($this->seenUrls[$url])) {
            return;
        }

        if ($depth === $this->depth) {
            return;
        }
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $html = curl_exec($ch);
        $info = curl_getinfo($ch);
        $info['referrer'] = $referrer;
        $info['depth'] = $depth;
        curl_close($ch);
        $this->seenUrls[$url] = true;

        yield $info;

        $crawler = new DomCrawler($html, $url);

        foreach ($crawler->filterXpath('//a') as $link) {
            $linkUrl = $link->getAttribute('href');

            $urlInfo = parse_url($linkUrl);

            if (isset($urlInfo['scheme'])) {
                continue;
            }

            $linkUrl = Path::join($this->url, $link->getAttribute('href'));

            yield from $this->crawlUrl($linkUrl, $url, $depth + 1);
        }

    }
}
